
response = """
<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
   <soapenv:Body>
      <ActivateSubscriberResultMsg xmlns="http://www.huawei.com/bme/aaainterface/cbs/businessmgrmsg">
         <ResultHeader xmlns="">
            <CommandId xmlns="http://www.huawei.com/bme/aaainterface/common">ActivateSubscriber</CommandId>
            <Version xmlns="http://www.huawei.com/bme/aaainterface/common">1</Version>
            <TransactionId xmlns="http://www.huawei.com/bme/aaainterface/common"/>
            <SequenceId xmlns="http://www.huawei.com/bme/aaainterface/common">1</SequenceId>
            <ResultCode xmlns="http://www.huawei.com/bme/aaainterface/common">405000000</ResultCode>
            <ResultDesc xmlns="http://www.huawei.com/bme/aaainterface/common">Operation successfully.</ResultDesc>
         </ResultHeader>
      </ActivateSubscriberResultMsg>
   </soapenv:Body>
</soapenv:Envelope>
"""

