#!/bin/bash

__conda_setup="$(CONDA_REPORT_ERRORS=false '/home/python/anaconda3/bin/conda' shell.bash hook 2> /dev/null)"
if [ $? -eq 0 ]; then
    \eval "$__conda_setup"
else
    if [ -f "/home/python/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/python/anaconda3/etc/profile.d/conda.sh"
        CONDA_CHANGEPS1=false conda activate base
    else
        \export PATH="/home/python/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup

conda activate aaa

gunicorn  wsgi -c gunicorn.config.py >> /dev/null &

pid=`cat /tmp/aaa.pid`
pid_count=`ps -ef|grep ${pid} |wc -l`
if [ ${pid_count} -ge 1]; then
  echo "startup done!"
else
  echo "startup fail!"


