from flask import Flask

import conf.config
from  utils.utils import log_handler
from conf.response_conf import *
import logging
from flask import (
    render_template,
    request,
    redirect,
    session,
    url_for,
    Blueprint,
    make_response,
)


app = Flask(__name__)
app.logger.addHandler(log_handler)
app.logger.setLevel(logging.DEBUG)

@app.route("/")
def index():
    # http://10.30.200.143:7782/services/AAAInterfaceBusinessMgrService.AAAInterfaceBusinessMgrServiceSOAP12port_http/
    form = request.form
    app.logger.info('request form: {}'.format( form))

    return "xxxx"


#@app.route("/services/AAAInterfaceBusinessMgrService.AAAInterfaceBusinessMgrServiceSOAP12port_http/",methods=["GET","POST"])
@app.route("/services/AAAInterfaceBusinessMgrService",methods=["GET","POST"])
def services():
    # http://10.30.200.143:7782/services/AAAInterfaceBusinessMgrService.AAAInterfaceBusinessMgrServiceSOAP12port_http/
    form = request.form
    app.logger.info('request from host: {}  method: {} '.format( request.remote_addr, request.method ))
    app.logger.info('request url: {}'.format( request.url))
    app.logger.info('request headers:{}'.format(request.headers))
    app.logger.info('request charset:{}'.format(request.charset))
    #app.logger.info('request headers:{}'.format(dir(request)))
    app.logger.info('request data: {}'.format( request.data.decode(request.charset)))

    return response

if __name__ == '__main__':
    # debug 模式可以自动加载你对代码的变动, 所以不用重启程序
    # host 参数指定为 '0.0.0.0' 可以让别的机器访问你的代码
    config = dict(
        debug=True,
        host='0.0.0.0',
        port=7782,


    )
    app.run(**config)

